from setuptools import setup

setup(name="aqms-ir", 
    version="0.8.3",
    description="Utilities to add FDSN StationXML (via obspy Inventory) to (part of) AQMS IR schema",
    url="https://gitlab.com/aqms-swg/aqms-ir",
    author="Renate Hartog, AQMS Software Working Group",
    license="MIT",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3,2.7',
        'Intended Audience :: Science/Research',
    ],
    packages=["aqms_ir"],
    scripts=["loadStationXML","getStationXML","deleteStation","closeStation"],
    install_requires=["numpy","obspy>=0.10.2","SQLAlchemy",],
    zip_safe=False)

